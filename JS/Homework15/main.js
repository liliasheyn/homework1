
// рекурсия - вызов функции самой себя. Использывается для упрощения решения задач. На подобие цикла

const userNum=+prompt('Введите число для вычисления факториала');
function factorial(n){
    if (n === 0) return 1;

    return n*factorial(n-1);
}

alert(factorial(userNum));