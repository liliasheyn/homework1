// Oбработчик событий-это функция,которая срабатывает как реакция на поисходящее.

let keyBox = document.getElementById("key");
let priceBox = document.getElementById("price-box");
let wrongPrice = document.getElementById("wrong-price");

function onfocus(e){
    keyBox.value = "";
    wrongPrice.innerHTML = "";
    priceBox.innerHTML = "";
    keyBox.style.outlineColor = "green";
    keyBox.style.color= "green";
} ;

function onblur(e) {
    if (keyBox.value >= 0) {
        keyBox.style.borderColor = "green";
        priceBox.insertAdjacentHTML("afterbegin", `<span>${keyBox.value}</span><span id="close">x</span>`);
    } else {
        keyBox.style.borderColor = "red";
        wrongPrice.insertAdjacentHTML("afterbegin", `<span>Please enter correct price</span>`);
    }
};

document.addEventListener('click', function (e) {
    if (e.target && e.target.id == 'close') {
        keyBox.value = "";
        priceBox.innerHTML = ""
    }
});


keyBox.addEventListener("change", onchange);
keyBox.addEventListener("focusout", onblur);
keyBox.addEventListener("focus", onfocus);
