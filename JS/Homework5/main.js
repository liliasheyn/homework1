// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Нужно экранировать спец символы ("", \, / ) и тд, потому что они могут попадаться в обычном тексте. При написании кода
// в дальнейшем может выпадать ошибки, типа "не закрыты кавычки" и тд

function createNewUser () {
    const userName = prompt('Input your name');
    const userSurname = prompt('Input your surname');
    const userBirthDay = prompt('Input your BDay (dd.mm.yyyy)');

    const newUser = {
        userName,
        userSurname,
        userBirthDay,
        getLogin() {
            return (this.userName[0] + this.userSurname).toLowerCase()
        },
        getAge () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let inputDate = +this.userBirthDay.substring(0, 2);
            let inputMonth = +this.userBirthDay.substring(3, 5);
            let inputYear = +this.userBirthDay.substring(6, 10);

            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;

            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age--;
            }
            return this.getAge();
        },
        getPassword() {
            return this.userName[0].toUpperCase() + this.userName.toLowerCase() + this.userBirthDay.substring(6, 10);

        }
    };

    return newUser;
}
const user = createNewUser();
// console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());


//
// function createNewUser(){
//
//     let newUser = {
//
//         firstName: prompt("Enter your first name"),
//         lastName: prompt("Enter last name"),
//         bDay: prompt ("Enter your birthday", 'dd.mm.yyyy'),
//
//
//         getLogin(){
//             return this.firstName.slice(0,1).toLowerCase() + this.lastName.toLowerCase();
//         },
//
//         getAge(){
//             return Math.round((new Date().getTime() - new Date(this.bDay)) / (24 * 3600 * 365 * 1000)) ;
//         },
//
//         getPassword(){
//             return this.firstName.slice(0,1).toUpperCase() + this.lastName.toLowerCase() + this.bDay.toString().slice(6)
//         }
//     };
//     return newUser
// }
// let user = createNewUser();
//
// console.log(user.getLogin());
// console.log(user.getAge());
// console.log(user.getPassword());
